package com.ahmad.rest.sample.resources;

import com.ahmad.rest.sample.models.Messages;
import com.ahmad.rest.sample.services.MessageService;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/messages")
public class MessageResource {

    MessageService msgService = new MessageService();

    /**
     * Getting all messages
     *
     * @return message as a String
     * @see Produces An Annotate the specifying response format
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Messages> getMessages() {
        return msgService.getAllMessages();
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{messageId}")
    public Messages getMessage(@PathParam("messageId") long id) {
        return msgService.getMessages(id);
    }

    /**
     * Adding Message
     *
     * @return message as a String
     * @see Produces An Annotate the specifying response format
     * @see Consumes An Annotate to specify the expected request body format
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Messages addMessage(Messages msg) {
        return msgService.addMessage(msg);
    }

    @PUT
    @Path("/{messageId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Messages updateMessage(@PathParam("messageId") long id, Messages msg) {
        msg.setId(id);
        return msgService.updateMessage(msg);
    }

    @DELETE
    @Path("/{messageId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteMessage(@PathParam("messageId") long id) {
        msgService.removeMessage(id);
    }


}
