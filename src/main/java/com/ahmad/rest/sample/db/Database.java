package com.ahmad.rest.sample.db;

import com.ahmad.rest.sample.models.Messages;
import com.ahmad.rest.sample.models.Profile;

import java.util.HashMap;
import java.util.Map;

public class Database {

    private static Map<Long, Messages> message = new HashMap<>();
    private static Map<Long, Profile> profile = new HashMap<>();

    public static Map<Long, Messages> getMessage() {
        return message;
    }

    public static Map<Long, Profile> getProfile() {
        return profile;
    }
}
