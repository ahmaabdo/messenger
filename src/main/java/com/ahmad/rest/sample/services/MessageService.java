package com.ahmad.rest.sample.services;

import com.ahmad.rest.sample.db.Database;
import com.ahmad.rest.sample.models.Messages;
import com.ahmad.rest.sample.models.Profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MessageService {

    private Map<Long, Messages> messages = Database.getMessage();
    private Map<Long, Profile> profiles = Database.getProfile();

    public MessageService() {
    }

    public List<Messages> getAllMessages() {
        return new ArrayList<Messages>(messages.values());
    }

    public Messages getMessages(long id) {
        return messages.get(id);
    }

    public Messages addMessage(Messages msg) {
        msg.setId(messages.size() + 1);
        messages.put(msg.getId(), msg);
        return msg;
    }

    public Messages updateMessage(Messages msg) {
        if (msg.getId() <= 0) {
            return null;
        }
        messages.put(msg.getId(), msg);
        return msg;
    }

    public Messages removeMessage(long id) {
        return messages.remove(id);
    }
}
